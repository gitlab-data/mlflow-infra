#!/usr/bin/env bash

set -e

# Verify that all required variables are set
if [[ -z "${MLFLOW_TRACKING_USERNAME}" ]]; then
    echo "Warning: MLFLOW_TRACKING_USERNAME not set"
    export MLFLOW_TRACKING_USERNAME=''
fi

if [[ -z "${MLFLOW_TRACKING_PASSWORD}" ]]; then
    echo "Warning: MLFLOW_TRACKING_PASSWORD not set"
    export MLFLOW_TRACKING_PASSWORD=''
fi

if [[ -z "${MLFLOW_ARTIFACT_REGISTRY_URL}" ]]; then
    echo "Warning: MLFLOW_ARTIFACT_REGISTRY_URL not set"
    export MLFLOW_ARTIFACT_REGISTRY_URL=''
fi

if [[ -z "${MLFLOW__CORE__SQL_ALCHEMY_CONN}" ]]; then
    echo "Warning: MLFLOW__CORE__SQL_ALCHEMY_CONN not set"
    export MLFLOW__CORE__SQL_ALCHEMY_CONN=''
fi

if [[ -z "${PORT}" ]]; then
    export PORT=5000
fi

if [[ -z "${HOST}" ]]; then
    export HOST=0.0.0.0
fi

export WSGI_AUTH_CREDENTIALS="${MLFLOW_TRACKING_USERNAME}:${MLFLOW_TRACKING_PASSWORD}"
export _MLFLOW_SERVER_ARTIFACT_ROOT="${MLFLOW_ARTIFACT_REGISTRY_URL}"
export _MLFLOW_SERVER_FILE_STORE="${MLFLOW__CORE__SQL_ALCHEMY_CONN}"

# Start MLflow and ngingx using supervisor
exec gunicorn -b "${HOST}:${PORT}" -w 4 --log-level debug --access-logfile=- --error-logfile=- --log-level=debug mlflow_auth:app
