provider "kubernetes" {
  config_path    = "~/.kube/config"
}

provider "kubectl" {
  config_path    = "~/.kube/config"
}

data "kubectl_filename_list" "deploy_manifests" {
    pattern = "./deploy_manifests/*.yaml"
}

resource "kubectl_manifest" "deployment_list" {
    count     = length(data.kubectl_filename_list.deploy_manifests.matches)
    yaml_body = file(element(data.kubectl_filename_list.deploy_manifests.matches, count.index))
}

data "kubectl_filename_list" "env_manifests" {
    pattern = "./deploy_manifests/*.yaml"
}

resource "kubectl_manifest" "env_list" {
    count     = length(data.kubectl_filename_list.env_manifests.matches)
    yaml_body = file(element(data.kubectl_filename_list.env_manifests.matches, count.index))
}